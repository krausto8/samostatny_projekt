# Návrh a testování generátoru pseudonáhodných čísel


## Popis projektu
Student se seznámí s metodami návrhu a testování generátoru pseudonáhodných čísel (PRNG) založeného na chaosu a jeho implementací v prostředí Matlab.

Hlavní náplní práce je návrh a testování PRNG založeného na chaosu a jeho implementace pomocí softwaru Matlab. Nedílnou součástí práce bude analýza možností vytvoření grafického uživatelského rozhraní pro nastavování PRNG v nástroji Matlab App Designer a jeho hostovaní využitím např. produktu Matlab Web App Server.

## Popis aplikace
Aplikace slouží ke generování pseudonáhodné sekvence čísel. Tlačítkem **Generate** se vygeneruje .txt soubor s posloupností, tlačítkem **Plot** se vykreslí 3D model dané posloupnosti.

## Spuštění aplikace
Ke spustění aplikace stačí mít nainstalován MATLAB. Aplikaci pak spustíme poklikáním. Pro editaci aplikace je potřeba jí otevřít přímo v prostředí MATLAB.
